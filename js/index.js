// HTML ELMENTS
const cards = document.querySelectorAll(".card");

// Matrix
let mat = new Array(3).fill(0).map(() => new Array(3).fill(-1));

let activePlayer = "A";
let turn = 0;

// add click event listener to all boxes
// cards.forEach((card) =>
//   card.addEventListener(
//     "click",
//     (event) => {
//       move(event);
//     },
//     { once: true }
//   )
// );

// display first player move
showPlayerMove();

// Move
function move(id) {
  //let id = e.target.getAttribute("id");

  drawMarker(id);

  const { row, col } = getRowCol(id);

  setMat(row, col);

  if (checkRow(row) || checkCol(col) || checkDiag()) {
    if (turn % 2 == 0) {
      document.querySelector(".winner").innerText = "player 1 won";
    } else {
      document.querySelector(".winner").innerText = "player 2 won";
    }
    clearAll();
    // document.querySelector(".btn").style.visibility = "visible";
    // document.getElementById("player-move").innerText = "";
  }
  showPlayerMove();
  turn += 1;
}

// Set Matrix Data
function setMat(row, col) {
  if (turn % 2 == 0) mat[row][col] = 1;
  else mat[row][col] = 2;
}

// Get Row-Col from Grid Id
function getRowCol(id) {
  let row = id / 3;
  let col = (id + 3) % 3;
  return { row: Math.trunc(row), col: col };
}

//check Row
function checkRow(rowId) {
  //console.log(mat[0][rowId], mat[1][rowId], mat[2][rowId]);
  if (
    mat[rowId][0] != -1 &&
    mat[rowId][0] == mat[rowId][1] &&
    mat[rowId][1] == mat[rowId][2]
  ) {
    //console.log("matched");
    return true;
  } else {
    return false;
  }
}

//check Diag
function checkDiag() {
  if (mat[0][0] != -1 && mat[0][0] == mat[1][1] && mat[1][1] == mat[2][2]) {
    console.log("matched");
    return true;
  } else if (
    mat[0][2] != -1 &&
    mat[0][2] == mat[1][1] &&
    mat[1][1] == mat[2][0]
  ) {
    console.log("matched");
    return true;
  }

  //console.log("umatched");
  return false;
}

//check Col
function checkCol(colId) {
  if (
    mat[0][colId] != -1 &&
    mat[0][colId] == mat[1][colId] &&
    mat[1][colId] == mat[2][colId]
  ) {
    console.log("matched");
    return true;
  } else {
    //console.log("unmatched");
    return false;
  }
}

function drawMarker(id) {
  if (turn % 2 == 0) document.getElementById(id).innerText = "X";
  else document.getElementById(id).innerText = "O";
}

function showPlayerMove() {
  if (turn % 2 == 0)
    document.getElementById("player-move").innerText = "Player 1 Move";
  else document.getElementById("player-move").innerText = "Player 2 Move";
}

function clearAll() {
  document.querySelector(".btn").style.visibility = "visible";
  document.getElementById("player-move").style.visibility = "hidden";
  let cards = document.querySelectorAll(".card");
  cards.forEach(function (card) {
    card.removeAttribute("onclick");
  });
}
